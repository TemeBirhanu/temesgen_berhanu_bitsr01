var typed = new Typed(".typing", {
    strings: ["","Software Engineer", "Web Designer", "Web Developer", "Mobile App Developer","Graphic Designer"],
    typeSpeed: 100,
    BackSpeed: 60,
    loop: true
});